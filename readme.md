# Version

**TemperChart**

## Installation

download or clone the project 

Run the composer to install dependencies [Composer](https://getcomposer.org/):

    composer install

Now run yarn or npm install to install node dependencies:

    npm install or yarn

Now run migrations to create tables and seed data:
     
     php artisan migrate --seed
     
Run npm to build resource files:
     
     npm run dev
Generate application key:
       
       php artisan key:generate
   
Go to <site_url>/home to see the chart.   
  
 ## License
 
 The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
