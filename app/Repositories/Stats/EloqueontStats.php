<?php

namespace App\Repositories\Stats;

use App\Stat;
use Carbon\Carbon;
use App\Http\Requests\StatsRequest;

class EloqueontStats implements StatsRepository
{

    /**
     * @var Stat
     */
    private $model;

    public function __construct(Stat $model)
    {
        $this->model = $model;
    }

    public function getRecordsBetween(Carbon $from, Carbon $to)
    {
        return $this->model->whereBetween('created_at', [$from->subDay(), $to->addDay()])->get();
    }

    public function getOldest()
    {
        return $this->model->oldest()->first()->created_at;
    }

    public function getLatest()
    {
        return $this->model->latest()->first()->created_at;
    }

    public function getStartDate(StatsRequest $request)
    {
        return Carbon::parse($request->get('first', $this->getOldest()));
    }

    public function getEndDate(StatsRequest $request)
    {
        return Carbon::parse($request->get('last', $this->getLatest()));
    }
}