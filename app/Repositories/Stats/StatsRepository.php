<?php

namespace App\Repositories\Stats;

use Carbon\Carbon;
use App\Http\Requests\StatsRequest;

interface StatsRepository
{
    public function getOldest();

    public function getLatest();

    public function getRecordsBetween(Carbon $from, Carbon $to);

    public function getStartDate(StatsRequest $request);

    public function getEndDate(StatsRequest $request);
}