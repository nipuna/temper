<?php

namespace App\Transformers;

use App\Repositories\Stats\StatsRepository;

interface StatsTransformerInterface
{
    public function getCohortsStatsSeries(StatsRepository $stats, array $weeks);

    public function getXaxisValues();
}