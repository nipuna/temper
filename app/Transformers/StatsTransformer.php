<?php

namespace App\Transformers;

use App\Repositories\Stats\StatsRepository;
use Illuminate\Support\Collection;

class StatsTransformer implements StatsTransformerInterface
{
    public function getCohortsStatsSeries(StatsRepository $stats, array $weeks)
    {
        $series = [];
        foreach ($weeks as $week) {
            $statsOfWeek = $stats->getRecordsBetween($week['start'], $week['end']);
            $usersCount = $statsOfWeek->groupBy('user_id')->count();
            $series[] = $this->getWeeklyCohortsPercentage($statsOfWeek, $usersCount);
        }

        return $series;
    }

    public function getWeeklyCohortsPercentage(Collection $statsOfWeek, $usersCount)
    {
        return collect($this->getXaxisValues())->map(function ($step) use ($statsOfWeek, $usersCount) {
            if (!$usersCount) {
                return null;
            }
            $value = round((100 / $usersCount) * $statsOfWeek->where('percentage', '>=', $step)->count(), 2);

            return ($value) ?: null;
        })->all();
    }

    public function getXaxisValues()
    {
        return [0, 20, 40, 45, 50, 70, 75, 90, 99, 100];
    }
}