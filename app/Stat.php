<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stat extends Model
{
    protected $table = 'stats';

    protected $fillable = ['user_id', 'percentage'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
