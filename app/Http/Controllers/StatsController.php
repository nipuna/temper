<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatsRequest;
use App\Repositories\Stats\StatsRepository;
use App\Support\DateTimeUtility;
use App\Transformers\StatsTransformerInterface;

class StatsController extends Controller
{
    /**
     * @var StatsRepository
     */
    private $stats;

    /**
     * @var StatsTransformerInterface
     */
    private $statsTransformer;

    /**
     * @param StatsRepository $stats
     * @param StatsTransformerInterface $statsTransformer
     */
    public function __construct(StatsRepository $stats, StatsTransformerInterface $statsTransformer)
    {
        $this->stats = $stats;
        $this->statsTransformer = $statsTransformer;
    }

    /**
     * @param StatsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(StatsRequest $request)
    {
        $weeks = DateTimeUtility::splitInToWeeks($this->stats->getStartDate($request), $this->stats->getEndDate($request));

        return response()->json(
            [
                'xaxis' => $this->statsTransformer->getXaxisValues(),
                'series' => $this->statsTransformer->getCohortsStatsSeries($this->stats, $weeks),
                'legends' => DateTimeUtility::getWeekOfTheYear($weeks)
            ]
        );
    }
}
