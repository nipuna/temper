<?php

namespace App\Support;

use Carbon\Carbon;

class DateTimeUtility
{
    public static function splitInToWeeks($firstDate, $lastDate)
    {
        $from = $firstDate;
        $to = Carbon::parse($firstDate)->addWeek();
        $weeks[1] = [
            'start' => $from,
            'end' => $to
        ];
        while ($to->lt($lastDate)) {
            $weeks[] = [
                'start' => $to,
                'end' => $to = Carbon::parse($to)->addWeek()
            ];
        }

        return $weeks;
    }

    public static function getWeekOfTheYear($weeks)
    {
        $weekOfTheYear = [];
        foreach ($weeks as $week) {
            $weekOfTheYear[] = "week {$week['start']->weekOfYear}";
        }

        return $weekOfTheYear;
    }
}