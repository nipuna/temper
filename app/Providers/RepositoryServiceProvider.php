<?php

namespace App\Providers;

use App\Repositories\Stats\EloqueontStats;
use App\Repositories\Stats\StatsRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any repository services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind(StatsRepository::class, EloqueontStats::class);
    }
}
