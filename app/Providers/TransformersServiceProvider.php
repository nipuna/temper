<?php

namespace App\Providers;

use App\Transformers\StatsTransformerInterface;
use App\Transformers\StatsTransformer;
use Illuminate\Support\ServiceProvider;

class TransformersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any transformer services.
     *
     * @return void
     */
    public function register()
    {
       $this->app->bind(StatsTransformerInterface::class, StatsTransformer::class);
    }
}
