<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Stat;

class StatsControllerTest extends TestCase
{
	use DatabaseMigrations;
    use DatabaseTransactions;

	public function setUp()
    {
        parent::setUp();

        $userIds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        collect($userIds)->each(function ($id) {
            factory(User::class)->create(['id' => $id]);
        });
        $statData = [
            ['user_id' => 1, 'percentage' => 20, 'created_at' => '2017-07-19 00:00:00'],
            ['user_id' => 2, 'percentage' => 20, 'created_at' => '2017-07-19 00:00:00'],
            ['user_id' => 3, 'percentage' => 40, 'created_at' => '2017-07-19 00:00:00'],
            ['user_id' => 4, 'percentage' => 40, 'created_at' => '2017-07-20 00:00:00'],
            ['user_id' => 5, 'percentage' => 50, 'created_at' => '2017-07-20 00:00:00'],
            ['user_id' => 6, 'percentage' => 50, 'created_at' => '2017-07-21 00:00:00'],
            ['user_id' => 7, 'percentage' => 70, 'created_at' => '2017-07-22 00:00:00'],
            ['user_id' => 8, 'percentage' => 90, 'created_at' => '2017-07-23 00:00:00'],
            ['user_id' => 9, 'percentage' => 90, 'created_at' => '2017-07-24 00:00:00'],
            ['user_id' => 10, 'percentage' => 100, 'created_at' => '2017-07-25 00:00:00'],
            ['user_id' => 10, 'percentage' => 100, 'created_at' => '2017-07-26 00:00:00'],
            ['user_id' => 10, 'percentage' => 100, 'created_at' => '2017-07-27 00:00:00'],
        ];
        Stat::insert($statData);
    }

    public function testFetchAllFromDbTable()
    {
        $response = $this->get('/api/stats');
        $response->assertStatus(200);
        $responseJson = $response->json();
        $this->assertCount(3, $responseJson);
        $this->assertCount(2, $responseJson['series']);
    }

    public function testFetchBasedOnGivenRange()
    {
        $response = $this->get('/api/stats?first=2017-07-19&last=2017-07-25');
        $response->assertStatus(200);
        $responseJson = $response->json();
        $this->assertCount(3, $responseJson);
        $this->assertCount(1, $responseJson['series']);
    }
}