<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Repositories\Stats\EloqueontStats;
use App\User;
use App\Stat;

class StatsRepositoryTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    private $eloqueontStats;

    public function setUp()
    {
        parent::setUp();

        $userIds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        collect($userIds)->each(function ($id) {
            factory(User::class)->create(['id' => $id]);
        });
        $statData = [
            ['user_id' => 1, 'percentage' => 20, 'created_at' => '2017-07-19 00:00:00'],
            ['user_id' => 2, 'percentage' => 20, 'created_at' => '2017-07-19 00:00:00'],
            ['user_id' => 3, 'percentage' => 40, 'created_at' => '2017-07-19 00:00:00'],
            ['user_id' => 4, 'percentage' => 40, 'created_at' => '2017-07-20 00:00:00'],
            ['user_id' => 5, 'percentage' => 50, 'created_at' => '2017-07-20 00:00:00'],
            ['user_id' => 6, 'percentage' => 50, 'created_at' => '2017-07-21 00:00:00'],
            ['user_id' => 7, 'percentage' => 70, 'created_at' => '2017-07-22 00:00:00'],
            ['user_id' => 8, 'percentage' => 90, 'created_at' => '2017-07-23 00:00:00'],
            ['user_id' => 9, 'percentage' => 90, 'created_at' => '2017-07-24 00:00:00'],
            ['user_id' => 10, 'percentage' => 100, 'created_at' => '2017-07-25 00:00:00'],
        ];
        Stat::insert($statData);
        $this->eloqueontStats = new EloqueontStats(new Stat());
    }

    public function testGetOldest()
    {
        $date = $this->eloqueontStats->getOldest();
        $this->assertEquals('2017-07-19', $date->toDateString());
    }

    public function testGetLatest()
    {
        $date = $this->eloqueontStats->getLatest();
        $this->assertEquals('2017-07-25', $date->toDateString());
    }

    public function testGetRecordsBetween()
    {
        $records = $this->eloqueontStats->getRecordsBetween(
            Carbon::create('2017', '07', '19'),
            Carbon::create('2017', '07', '25')
        )->toArray();

        $this->assertCount(10, $records);
        $this->assertArrayHasKey(0, $records);
        $this->assertArrayHasKey(9, $records);
        $this->assertEquals(20, $records[0]['percentage']);
        $this->assertEquals('2017-07-25 00:00:00', $records[9]['created_at']);
    }
}
