<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Support\DateTimeUtility;
use App\User;
use App\Stat;

class DateTimeUtilityTest extends TestCase
{
    public function testSplitInToWeeks()
    {
        $from = Carbon::parse('2016-07-19 00:00:00.000000');
        $to = Carbon::parse('2016-08-02 00:00:00.000000');
        $weeks = DateTimeUtility::splitInToWeeks($from, $to);
        $this->assertCount(2, $weeks);
        $this->assertEquals($from->addWeek(), $weeks[1]['end']);
        $this->assertEquals($to, $weeks[2]['end']);
    }
}
